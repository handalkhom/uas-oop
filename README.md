<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

-   [Simple, fast routing engine](https://laravel.com/docs/routing).
-   [Powerful dependency injection container](https://laravel.com/docs/container).
-   Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
-   Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
-   Database agnostic [schema migrations](https://laravel.com/docs/migrations).
-   [Robust background job processing](https://laravel.com/docs/queues).
-   [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 2000 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

-   **[Vehikl](https://vehikl.com/)**
-   **[Tighten Co.](https://tighten.co)**
-   **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
-   **[64 Robots](https://64robots.com)**
-   **[Cubet Techno Labs](https://cubettech.com)**
-   **[Cyber-Duck](https://cyber-duck.co.uk)**
-   **[Many](https://www.many.co.uk)**
-   **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
-   **[DevSquad](https://devsquad.com)**
-   **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
-   **[OP.GG](https://op.gg)**
-   **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
-   **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

# JAWABAN UAS-OOP

# No. 1

Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

-   Use case user

| Use Case                                         | Nilai Prioritas |
| ------------------------------------------------ | --------------- |
| User dapat mengirim pesan ke user terdaftar lain | 10              |
| User dapat mengirim file ke user terdaftar lain  | 10              |
| User dapat mengubah foto profile                 | 10              |
| User dapat menghapus pesan yang telah dikirim    | 10              |
| User dapat melakukan register akun               | 8               |
| User dapat melakukan login akun                  | 9               |
| User dapat melakukan logout akun                 | 7               |

-   Use case manajemen perusahaan

| Use Case                                              | Nilai Prioritas |
| ----------------------------------------------------- | --------------- |
| Admin dapat menghapus atau menambahkan akun           | 7               |
| Admin dapat menghapus atau menambahkan pesan tertentu | 6               |
| Admin dapat melihat room chat user                    | 6               |

-   Use case direksi perusahaan (dashboard, monitoring, analisis)

| Use Case                                         | Nilai Prioritas |
| ------------------------------------------------ | --------------- |
| Admin dapat melihat jumlah user                  | 8               |
| Admin dapat melihat jumlah room chat             | 8               |
| Admin dapat melihat kapan sebuah pesan dikirim   | 8               |
| Admin dapat melihat di mana sebuah pesan dikirim | 8               |

# No. 2

Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

- User dapat mengirim file ke user terdaftar lain 
- User dapat mengirim pesan ke user terdaftar lain
- User dapat menghapus pesan yang telah dikirim   

```mermaid
    classDiagram
        class ChatController {
            +kirim_pesan(Request $request)
            +kirim_file(Request $request)
            +new_chat(Request $request)
            +hapus_pesan(Request $request)
        }

        class Request {
            +$chat
            +$file
            +$room_id
            +$pengirim_id
            +$penerima_id
            +$token
            +file(string $key = null, mixed $default = null)
        }

        class Helper {
            +getUserLogin(string $token)
        }

        class DB {
            +table(string $table)
        }

        class Response {
            +json(array $data, int $status = 200, array $headers = [], int $options = 0)
        }

        class Illuminate {
            class Http {
                class Controllers {
                    class Controller {
                        // Controller class details
                    }
                }
            }
        }

        class UserRoom {
            +user_id
            +room_id
        }

        class Room {
            +name
        }

        class Chat {
            +isi
            +file
            +room_id
            +pengirim_id
            +chat_id
        }

        classUploadedFile {
            +getClientOriginalExtension()
            +move(string $destinationPath, string $fileName)
        }

        class Collection {
            +insert(array $data)
            +insertGetId(array $data)
            +where(string $column, $operator = null, $value = null, string $boolean = 'and')
            +get()
            +delete()
        }

        ChatController --> Request
        ChatController --> Helper
        ChatController --> DB
        ChatController --> Response
        ChatController --> Illuminate
        ChatController --> UserRoom
        ChatController --> Room
        ChatController --> Chat
        ChatController --> UploadedFile

        Request --> Helper
        Request --> UploadedFile

        Helper --> UserRoom

        DB --> Collection

        Chat --> Room
        Chat --> Collection
```

- User dapat melakukan register akun              
- User dapat melakukan login akun                 
- User dapat melakukan logout akun

```mermaid
classDiagram
    class AuthController {
        +authenticate(request: Request): Response
        +signup(request: Request): Response
    }
    class Request
    class Response
    class Auth {
        +attempt(credentials: array): bool
    }
    class Str {
        +random(length: int): string
    }
    class DB {
        +table(tableName: string): Table
    }
    class Table {
        +where(column: string, value: mixed): QueryBuilder
        +insert(data: array): bool
        +update(column: string, value: mixed): bool
    }
    class Hash {
        +make(value: string): string
    }

    AuthController --|> Controller
    AuthController --> Request
    AuthController --> Response
    AuthController --> Auth
    AuthController --> Str
    AuthController --> DB
    Auth --|> Facades\Auth
    Str --|> Facades\Str
    DB --|> Facades\DB
    Hash --|> Facades\Hash
```

- User dapat mengubah foto profile                
- User dapat melihat recent chat dengan user lain
- User dapat melihat history chat dengan user lain

```mermaid
classDiagram
    class HomeController {
        +dashboard(request: Request): JsonResponse
        +recentmessage(request: Request): JsonResponse
        +chathistory(request: Request): JsonResponse
        +update_profile_img(request: Request): JsonResponse
    }

    class Request {
        +token: string
        +search: string
        +room_id: string
        +file: UploadedFile
        +(...other properties...)
    }

    class JsonResponse {
        +data: array
        +status: string
    }

    class Helper {
        +getUserLogin(token: string): object
    }

    class DB {
        +table(table: string): object
        +raw(query: string): object
    }

    class Illuminate.Http.UploadedFile {
        +getClientOriginalExtension(): string
        +move(path: string, name: string): void
    }

    class user {
        +user_id: string
        +username: string
        +profile_img: string
        +(...other properties...)
    }

    class room {
        +room_id: string
        +(...other properties...)
    }

    class user_room {
        +user_id: string
        +room_id: string
    }

    class chat {
        +chat_id: string
        +room_id: string
        +isi: string
        +file: string
        +date_created: string
        +(...other properties...)
    }

    class user_room_tujuan {
        +user_id: string
        +room_id: string
    }

    class user_tujuan {
        +user_id: string
        +username: string
        +profile_img: string
        +(...other properties...)
    }

```

# No. 3

Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

-   Single Responsibility (SRP)

        Fungsi yang ditunjukkan hanya memiliki satu kegunaan

    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/3s.gif)

-   Open-Closed (OCP)

        Fungsi dan kelas yang ditunjukkan tertutup untuk modifikasi, namun terbuka untuk perluasan

    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/3o.gif)

-   Liskov Substitution (LSP)

        Objek dari kelas turunan harus dapat digunakan sebagai pengganti objek dari kelas induk tanpa mengubah kebenaran program

    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/3l.gif)

-   Interface Segregation (ISP)

        Klien tidak boleh dipaksa bergantung pada antarmuka yang tidak digunakan. Sebaliknya, antarmuka harus terpisah dan spesifik sesuai dengan kebutuhan klien

    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/3i.gif)

-   Dependency Inversion (DIP)

        Modul atau kelas tingkat tinggi tidak boleh bergantung pada modul atau kelas tingkat rendah

    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/3d.gif)

# No. 4

Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

Pada kode di atas, tidak terdapat penggunaan desain pattern yang khas. Namun, beberapa konsep dan komponen dari desain pattern dapat dilihat di dalamnya. Berikut adalah beberapa konsep dan komponen yang terdapat dalam kode tersebut:

1. Komponen React, contohnya:
   - Penggunaan fungsi `useState` dan `useEffect` untuk mengelola state dan efek samping.
   - Pembuatan komponen fungsional dengan menggunakan `React.FC`.

2. Komponen Ant Design, contohnya:
   - Penggunaan komponen seperti `Layout`, `List`, `Avatar`, `Input`, `Button`, dan `Dropdown` dari Ant Design.
   - Penggunaan properti seperti `itemLayout`, `dataSource`, `renderItem`, `onClick`, dan `icon` pada komponen-komponen tersebut.

3. Penanganan Input dan Aksi, contohnya:
   - Penggunaan fungsi `handleButtonClick` dan `handleMenuClick` untuk menangani klik tombol dan menu.
   - Penggunaan `message.info` untuk menampilkan pesan informasi.

4. Penggunaan Axios, contohnya:
   - Penggunaan library Axios untuk melakukan permintaan HTTP ke backend melalui fungsi `axios.get`.

Meskipun tidak ada penggunaan desain pattern khusus dalam kode ini, konsep-konsep tersebut dapat membantu dalam mengorganisir dan mengelola logika aplikasi.

![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/4.gif)

# No. 5

Mampu menunjukkan dan menjelaskan konektivitas ke database
![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/5.gif)

# No. 6

Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

-   Create
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6c.gif)
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6cr.gif)

-   Read
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6r.gif)
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6re.gif)

-   Update
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6u.gif)
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6up.gif)

-   Delete
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6d.gif)
    ![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/6de.gif)

# No. 7

Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/7.gif)

# No. 8

Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/8a.gif)
![](https://gitlab.com/handalkhom/uas-oop/-/raw/main/gif/7.gif)

# No. 9

Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

Link Youtube: https://youtu.be/zPZ7l1dXOyo

# No. 10

Bonus: Mendemonstrasikan penggunaan Machine Learning pada produk yang digunakan
![]()
