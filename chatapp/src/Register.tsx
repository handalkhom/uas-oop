import React from "react";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Layout, theme, Button, Form, Input } from "antd";
import { Link } from "react-router-dom";
import axios from "axios";

const { Header, Content, Footer } = Layout;
const host = "http://127.0.0.1:8000";

const onFinish = (values: any) => {
    const data = {
        username: values.username,
        password: values.password,
        conf_password: values.confirm,
    };
    axios.post(host + "/api/register", data).then(function (result) {
        // chatArr = result;
        // console.log(result);
        const data = result.data;
        if (data.status == "success") {
            alert(data.message);
            document.location.href = "/";
        } else {
            alert(data.message);
        }
    });
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const Register: React.FC = () => {
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    return (
        <Layout>
            <Header
                style={{
                    position: "sticky",
                    top: 0,
                    zIndex: 1,
                    width: "100%",
                    display: "flex",
                    alignItems: "center",
                    fontWeight: "bold",
                    color: "White",
                }}
            >
                REGISTER
            </Header>
            <Content
                className="site-layout"
                style={{
                    padding: "50px",
                    display: "flex",
                    justifyContent: "center",
                }}
            >
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    style={{
                        padding: "50px",
                        maxWidth: 600,
                        width: "100%",
                        // margin: "50px 30%",
                    }}
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: "Please input your Username!",
                            },
                        ]}
                    >
                        <Input
                            prefix={
                                <UserOutlined className="site-form-item-icon" />
                            }
                            placeholder="Username"
                        />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: "Please input your password!",
                            },
                        ]}
                        hasFeedback
                    >
                        <Input.Password
                            prefix={
                                <LockOutlined className="site-form-item-icon" />
                            }
                            type="password"
                            placeholder="Password"
                        />
                    </Form.Item>

                    <Form.Item
                        name="confirm"
                        dependencies={["password"]}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: "Please confirm your password!",
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (
                                        !value ||
                                        getFieldValue("password") === value
                                    ) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(
                                        new Error(
                                            "The new password that you entered do not match!"
                                        )
                                    );
                                },
                            }),
                        ]}
                    >
                        <Input.Password
                            prefix={
                                <LockOutlined className="site-form-item-icon" />
                            }
                            type="password"
                            placeholder="Confirm Password"
                        />
                    </Form.Item>

                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">
                            Register
                        </Button>
                        Or <Link to="/">Login</Link>
                    </Form.Item>
                </Form>
            </Content>
            <Footer style={{ textAlign: "center" }}>
                Ant Design ©2023 Created by Ant UED
            </Footer>
        </Layout>
    );
};

export default Register;
