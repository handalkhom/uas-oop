<?php

namespace App\Helper;

class Helper{
    public static function getUserLogin($token){
        if(empty($token)){
            $return_arr = [
                "status"    => "error",
                "message"    => "User Login not found",
            ];

            echo json_encode($return_arr);
            exit;
        }
        return \DB::table("user")->where("remember_token",$token)->first();
    }
}